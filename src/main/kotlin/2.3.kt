/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/12/14
* TITLE: Nombre de dígits
*/
import java.util.*

fun digitsNumber (number:  Int) : Long {
    return if (number > 0) {
        digitsNumber(number / 10) + 1

    }
    else 0

}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero:")

    println(digitsNumber(scanner.nextInt()))
}