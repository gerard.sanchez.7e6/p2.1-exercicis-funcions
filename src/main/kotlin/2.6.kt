/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/12/14
* TITLE: Reducció de dígits
*/
import java.util.*

fun sequenceOfAsterisks (number:  Int) {
    if (1 < number){
        sequenceOfAsterisks(number-1)
    }
    for (i in 1 .. number){
        print("*")
    }
    println()
    if (number>1){
        sequenceOfAsterisks(number-1)
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero:")

    println(sequenceOfAsterisks(scanner.nextInt()))
}