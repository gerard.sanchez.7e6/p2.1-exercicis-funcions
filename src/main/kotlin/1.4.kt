/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/12/14
* TITLE:Subseqüència
*/
import java.util.*

fun stringSequense( userPhrase : String, userNumber : Int, secondUserNumber : Int ) : String {
    return if (userPhrase.length > userNumber && userPhrase.length >secondUserNumber){
        userPhrase.subSequence(userNumber,secondUserNumber).toString()
    }
    else {
        "La subseqüència $userNumber - $secondUserNumber de l'String no existeix"
    }
}
fun main() {
    val scanner = Scanner(System.`in`)
    println("Escriu una frase i el rang que voleu saber")
    println(stringSequense(scanner.nextLine(),scanner.nextInt(),scanner.nextInt()))
}