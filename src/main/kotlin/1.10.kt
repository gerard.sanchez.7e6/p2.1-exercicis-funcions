/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/12/14
* TITLE: Escriu un marc per un String
*/
import java.util.*

fun cross( userString : String, userChar : Char   ) {
    repeat(userString.length +4){
        print(userChar)
    }
    println()
    for ( i in 1 .. userString.length + 4){
        if (i == 1){
            print(userChar)
        }
        if ( i == userString.length + 3){
            print(userChar)
        }
        else{
            print(' ')
        }
    }
    println()
    println("$userChar $userString $userChar")

    for ( i in 1 .. userString.length + 4){
        if (i == 1){
            print(userChar)
        }
        if ( i == userString.length + 3){
            print(userChar)
        }
        else{
            print(' ')
        }
    }
    println()
    repeat(userString.length +4){
        print(userChar)
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Escriu un nombre enter i un caràcter:")
    return cross(scanner.nextLine(), scanner.next().single() )

}