/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/12/14
* TITLE: Eleva’l
*/
import java.util.*

fun powerResult( userNumber : Int, secondUserNumber : Int  ) : Long {
    var result = 1L

    for (i in 1.. secondUserNumber){
        result *= userNumber
    }
    return result
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Escriu dos nombres enters :")
    println("El resultat és : \n${powerResult(scanner.nextInt(),scanner.nextInt())}")

}