/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/12/14
* TITLE: Escriu una creu
*/
import java.util.*

fun cross( userNumber : Int , userChar : Char  ) {
    for ( i in 1 .. userNumber){
        if ((userNumber / 2) + 1 == i){
            repeat(userNumber){
                print(userChar)
            }
            println()
        }
        else {
            repeat((userNumber / 2 )){
                print(' ')
            }
           println(userChar)
        }
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Escriu un nombre enter i un caràcter:")
    return cross(scanner.nextInt(), scanner.next().single())

}