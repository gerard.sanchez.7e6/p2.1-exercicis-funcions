/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/12/14
* TITLE: Nombres creixents
*/
import java.util.*

fun increasingNumbers (number:  String) : Boolean {
    for (i in 0 until number.lastIndex) {
        return if (number[i] > number[i+1]){
            increasingNumbers(number.drop(1))
        }
        else{
            true
        }
    }
    return false
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero:")

    println(increasingNumbers(scanner.next()))
}