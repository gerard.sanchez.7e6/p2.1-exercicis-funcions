/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/12/14
* TITLE: String és igual
*/
import java.util.*

fun equals( userPhrase : String, secondUserPhrase : String  ) : Boolean{
    var result = true
    for ( i in 0 .. userPhrase.lastIndex){
        if (userPhrase[i] != secondUserPhrase[i]){
            result = false
            break
        }
    }
    return result
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Escriu dues paraules :")
    println("Són iguals? \n${equals(scanner.next(),scanner.next())}")

}