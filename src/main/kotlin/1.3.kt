/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/12/14
* TITLE:Caràcter d’un String
*/
import java.util.*

fun charString( userPhrase : String, userNumber : Int ) : String {
    return if (userPhrase.length > userNumber){
        userPhrase[userNumber].toString()
    }
    else {
        "La mida de l'String és inferior a $userNumber"
    }

}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Escriu una frase i la posició del caràcter que vols saber")
    println(charString(scanner.nextLine(),scanner.nextInt()))
}