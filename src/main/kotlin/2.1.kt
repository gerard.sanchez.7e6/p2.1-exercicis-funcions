/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/12/14
* TITLE: Factorial
*/
import java.util.*

fun factorial (number:  Int) : Long {
    return if (number > 1) {
        number * factorial(number-1)
    }
    else 1
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero:")


    println(factorial(scanner.nextInt()))
}
