/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/12/14
* TITLE: Mida d’un String
*/
import java.util.*

fun stringSize( phrase : String) : Int{
    var result = 0
    for (i in 1 .. phrase.length){
        result++
    }
    return result

}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Escriu una frase:")
    println("Aquesta és la seva mida : ${stringSize(scanner.nextLine())}")

}
