/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/12/14
* TITLE: Escriu una línia
*/
import java.util.*

fun rareString( userNumber : Int , secondUserNumber : Int, userChar : Char  ) : String{
    var result = ""

    repeat(userNumber){
        result += ' '
    }
    repeat(secondUserNumber){
        result += userChar
    }
    return result

}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Escriu dos nombres enters i un caràcter:")
    println("El resultat és : \n${rareString(scanner.nextInt(),scanner.nextInt(), scanner.next().single())}")

}