/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/12/14
* TITLE: Reducció de dígits
*/
import java.util.*

fun digitReduction (number:  Int) : Int {
    return if ( number < 10){
        number
    }
    else {
        digitReduction(number%10 + digitReduction(number/10))
    }

}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero:")

    println(digitReduction(scanner.nextInt()))
}