/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/12/14
* TITLE: D’String a MutableList<Char>
*/
import java.util.*

fun phrseList( userPhrase : String, userChar : Char  ) : MutableList<String>{
    val list : MutableList<String> = mutableListOf()
    list.add(userPhrase.split(userChar).toString())
    return list

}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Escriu una sequéncia de caràcters :")
    println("Aquesta és la seva llista? \n${phrseList(scanner.next(), scanner.next().single())}")

}