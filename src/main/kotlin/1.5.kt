/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/12/14
* TITLE: D’String a MutableList<Char>
*/
import java.util.*

fun wordList( userPhrase : String  ) : MutableList<Char>{
    val list : MutableList<Char> = mutableListOf()

    for (i in 0 .. userPhrase.lastIndex){
        list.add(userPhrase[i])
    }
    return list
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Escriu una paraula  :")
    println("Aquesta és la seva llista? \n${wordList(scanner.next())}")

}