/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 22/12/14
* TITLE: Doble factorial
*/
import java.util.*

fun doubleFactorial (number:  Int) : Long {
    return if (number > 1) {
        number * doubleFactorial(number-2)
    }
    else 1
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero:")

    println(doubleFactorial(scanner.nextInt()))
}